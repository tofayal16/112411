<?php

function odd($a) {
    return($a & 1);
}

function even($a) {
    return(!($a & 1));
}

$b = array("a" => 11, "b" => 18, "c" => 19, "d" => 5, "e" => 12);
$c = array(9, 2, 1, 5, 6, 7, 8, 6);
echo "Odd:<pre>";
print_r(array_filter($b, 'odd'));
echo '<pre>';
echo "<b>Even:</b><pre>";
print_r(array_filter($c, 'even'));
echo '<pre>';
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

